<?php

/**
 * @file
 * event_data_store.event_data_store.inc
 */

/**
 * Grab a list of all the available event triggers.
 */
function event_data_store_get_trigger_events() {
  $trigger_events = array(
    'disabled' => t('Disabled'),
    'user_registrations' => t('User registrations'),
    'user_logins' => t('User logins'),
    'user_logouts' => t('User logouts'),
    'users_blocked' => t('Users blocked'),
    'users_activated' => t('Users activated'),
    'users_cancelled' => t('Users cancelled'),
    'user_password_resets' => t('User password resets'),
    'nodes_created' => t('Nodes created'),
    'nodes_updated' => t('Nodes updated'),
    'nodes_deleted' => t('Nodes deleted'),
  );

  // Add in node type events.
  foreach (node_type_get_types() as $type_name => $type) {
    $replacements = array('@name' => $type->name);
    $trigger_events += array(
      "nodes_created_{$type_name}" => t('Nodes - "@name" nodes created', $replacements),
      "nodes_updated_{$type_name}" => t('Nodes - "@name" nodes updated', $replacements),
      "nodes_deleted_{$type_name}" => t('Nodes - "@name" nodes deleted', $replacements),
    );
  }

  // Add in comment events.
  if (module_exists('comment')) {
    $trigger_events += array(
      'comments_created' => t('Comments created'),
      'comments_deleted' => t('Comments deleted'),
    );
  }

  // Add in file events.
  if (module_exists('file')) {
    $trigger_events += array(
      'files_uploaded' => t('File uploaded'),
      'files_deleted' => t('Files deleted'),
    );
  }

  // Add in search events.
  if (module_exists('search')) {
    $trigger_events += array(
      'search_executed' => t('Search executed'),
    );
  }

  // Add in Harmony events.
  if (module_exists('harmony_core')) {
    $trigger_events += array(
      'harmony_threads_created' => t('Harmony Threads created'),
      'harmony_threads_updated' => t('Harmony Threads updated'),
      'harmony_threads_deleted' => t('Harmony Threads deleted'),
      'harmony_threads_created_category' => t('Harmony Threads created in category'),
      'harmony_threads_category__viewed' => t('Harmony Threads viewed in category'),
      'harmony_threads_category__replied' => t('Harmony Threads replied to in category'),
    );

    foreach (harmony_core_get_thread_types() as $type => $info) {
      $label = $type === 'harmony_thread' ? t('Default') : $info->label;
      $replacements = array('@label' => $label);
      $trigger_events += array(
        "harmony_threads_created_{$type}" => t('Harmony "@label" Threads created', $replacements),
        "harmony_threads_updated_{$type}" => t('Harmony "@label" Threads updated', $replacements),
        "harmony_threads_deleted_{$type}" => t('Harmony "@label" Threads deleted', $replacements),
      );
    }

    $trigger_events += array(
      'harmony_thread_view_tracked' => t('Harmony Thread view tracked'),
      'harmony_thread_view_tracked_auth' => t('Harmony Thread view tracked (authenticated)'),
      'harmony_thread_view_tracked_anon' => t('Harmony Thread view tracked (anonymous)'),
      'harmony_posts_created' => t('Harmony Posts created'),
      'harmony_posts_updated' => t('Harmony Posts updated'),
      'harmony_posts_deleted' => t('Harmony Posts deleted'),
    );
  }

  // Add Comstack PM events.
  if (module_exists('comstack_pm')) {
    $trigger_events += array(
      'comstack_conversations_created' => t('Comstack Conversations created'),
    );
  }

  // Add Message bundles.
  if (module_exists('message')) {
    foreach (message_type_load() as $message_type => $message_type_info) {
      $trigger_events["message_created_{$message_type}"] = t('Messages - "@label" messages created', array('@label' => $message_type_info->description));
    }
  }

  // Events for relationships.
  if (module_exists('user_relationships')) {
    foreach (user_relationships_types_load() as $relationship_id => $relationship_type) {
      // Get type name.
      $relationship_type_name = !empty($relationship_type->machine_name) ? $relationship_type->machine_name : user_relationships_type_get_name($relationship_type);

      $trigger_events["user_relationships__{$relationship_type_name}__request"] = t('User relationships - "@label" relationship requested', array('@label' => $relationship_type->name));
      $trigger_events["user_relationships__{$relationship_type_name}__approve"] = t('User relationships - "@label" relationship approved', array('@label' => $relationship_type->name));
      $trigger_events["user_relationships__{$relationship_type_name}__update"] = t('User relationships - "@label" relationship updated', array('@label' => $relationship_type->name));
      $trigger_events["user_relationships__{$relationship_type_name}__cancel"] = t('User relationships - "@label" relationship request cancelled', array('@label' => $relationship_type->name));
      $trigger_events["user_relationships__{$relationship_type_name}__disapprove"] = t('User relationships - "@label" relationship request rejected', array('@label' => $relationship_type->name));
      $trigger_events["user_relationships__{$relationship_type_name}__remove"] = t('User relationships - "@label" relationship removed', array('@label' => $relationship_type->name));
    }
  }

  /**
   * Allow other modules to define events.
   */
  foreach (module_implements('event_data_store_trigger_events') as $module) {
    $trigger_events[] = module_invoke($module, 'event_data_store_trigger_events');
  }

  return $trigger_events;
}

/**
 * Log an event.
 *
 * @param string $event
 *   The name of the event that is being logged.
 * @param string $entity_type
 *   The type of entity involved.
 * @param string $entity_id
 *   The ID of the entity that an action has occurred against.
 * @param array $data
 *   An array of additional data relevant to the event or entity.
 */
function event_data_store_log($event, $entity_type = '', $entity_id = '', $data = array()) {
  $events = entity_load('event_data_store_event', FALSE, array('event' => $event));

  if ($events) {
    global $user;
    $data = serialize($data);

    foreach ($events as $event_info) {
      db_insert('event_data_store')
        ->fields(array(
          'event_name' => $event_info->name,
          'uid' => $user->uid,
          'timestamp' => REQUEST_TIME,
          'user_agent' => !empty($_SERVER['HTTP_USER_AGENT']) ? substr($_SERVER['HTTP_USER_AGENT'], 0, 254) : '',
          'ip_address' => ip_address(),
          'entity_type' => $entity_type,
          'entity_id' => $entity_id,
          'data' => $data,
        ))
        ->execute();
    }
  }
}

/**
 * Helper function to create aggregate data for a specified interval.
 */
function event_data_store_cron_aggregate_interval($store, $interval, $past_hour, $update_previous = TRUE) {
  // Figure out the timestamp for the start of the day, week or month.
  switch ($interval) {
    case EVENT_DATA_STORE_AGGREGATED_INTERVAL_HOURLY:
      $date = mktime(date('H'), 0, 0);
      break;
    case EVENT_DATA_STORE_AGGREGATED_INTERVAL_DAILY:
      $date = mktime(0, 0, 0);
      break;
    case EVENT_DATA_STORE_AGGREGATED_INTERVAL_WEEKLY:
      // Gah, this gets complicated. Get first day of the week as set by admin.
      // Default to Monday. When using strtotime use the correct string for
      // last or this.
      $first_day_of_week = variable_get('date_first_day', 1);
      $php_date_week_format_char = variable_get('date_api_use_iso8601', 0) == 1 ? 'N' : 'w';
      if (variable_get('date_api_use_iso8601', 0) && $first_day_of_week === 0) {
        $first_day_of_week = 7;
      }
      $today_first_day_of_week = date($php_date_week_format_char) == $first_day_of_week;
      $use_this_or_last = $today_first_day_of_week ? 'this' : 'last';
      $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
      $date = strtotime($use_this_or_last . ' ' . $days[$first_day_of_week]);
      break;
    case EVENT_DATA_STORE_AGGREGATED_INTERVAL_MONTHLY:
      $date = mktime(0, 0, 0, date('n'), 1);
      break;
    case EVENT_DATA_STORE_AGGREGATED_INTERVAL_YEARLY:
      $date = mktime(0, 0, 0, 1, 1);
      break;
  }

  // Grab the current day, week or month record if one exists.
  $current_interval_record = db_query_range('SELECT * FROM {event_data_store_agg} WHERE event_name = :event_name AND period = :interval AND timestamp = :timestamp ORDER BY timestamp DESC', 0, 1, array(
      ':event_name' => $store->name,
      ':interval' => $interval,
      ':timestamp' => $date,
    ))
    ->fetchObject();

  // If a record exists, then update the flip out of it.
  if ($current_interval_record) {
    db_query('UPDATE {event_data_store_agg} SET value = :new_value WHERE edsa_id = :edsa_id', array(
      ':new_value' => ($current_interval_record->value + $past_hour),
      ':edsa_id' => $current_interval_record->edsa_id,
    ));
  }
  // First time round, create a record.
  else {
    // Make sure the previous interval is correct.
    $previous_interval_value = 0;
    if ($update_previous) {
      $previous_interval_value = event_data_store_finalise_previous_agg_value($store, $interval, $date);
    }

    // Do sum if that's the aggregation behaviour, final sum is taken care of
    // by the previous "finalise" function call.
    if ($store->aggregate_behaviour === 'sum') {
      $past_hour = $past_hour + $previous_interval_value;
    }

    // Create a record for the new interval.
    db_insert('event_data_store_agg')
      ->fields(array(
        'event_name' => $store->name,
        'timestamp' => $date,
        'value' => $past_hour,
        'period' => $interval,
      ))
      ->execute();
  }
}

/**
 * Just before we create a new aggregated record for an interval correct the
 * previous periods one as cron can be run at times which aren't dead on the
 * hour.
 */
function event_data_store_finalise_previous_agg_value($store, $interval, $date) {
  $number_to_grab = $store->aggregate_behaviour === 'tally' ? 1 : 2;
  $previous_interval_records = db_query_range('SELECT * FROM {event_data_store_agg} WHERE event_name = :event_name AND period = :interval ORDER BY timestamp DESC', 0, $number_to_grab, array(
    ':event_name' => $store->name,
    ':interval' => $interval,
  ))
  ->fetchAll();
  // If we've got a record or two, redo the count for the entire record.
  if (!empty($previous_interval_records)) {
    // Set our initial number.
    $previous_interval_count = $store->aggregate_behaviour === 'sum' && !empty($previous_interval_records[1]) ? $previous_interval_records[1]->value : 0;
    // Determine the end timestamp we want data to run up to.
    // This is just going to be the current date timestamp.
    $end_timestamp = $date;
    // Figure out the value for this time period.
    $previous_interval_count_from_db = db_query('SELECT COUNT(*) FROM {event_data_store} WHERE event_name = :event_name AND timestamp >= :timestamp AND timestamp < :end_timestamp', array(
      ':event_name' => $store->name,
      ':timestamp' => $previous_interval_records[0]->timestamp,
      ':end_timestamp' => $end_timestamp,
    ))->fetchField();
    if (!$previous_interval_count_from_db) {
      $previous_interval_count_from_db = 0;
    }
    $previous_interval_count = $previous_interval_count + $previous_interval_count_from_db;
    // Update the record.
    db_query('UPDATE {event_data_store_agg} SET value = :value WHERE edsa_id = :edsa_id', array(
      ':value' => $previous_interval_count,
      ':edsa_id' => $previous_interval_records[0]->edsa_id,
    ));

    return $previous_interval_count;
  }

  return 0;
}

/**
 * Construct the list of events to trigger on which are in use and save to a
 * variable.
 *
 * @return array An array of events to trigger on which are in use.
 */
function event_data_store_build_in_use() {
  $events = array();

  $listeners = event_data_store_event_load_all();
  if ($listeners) {
    foreach ($listeners as $key => $listener) {
      if (!in_array($listener->event, $events)) {
        $events[] = $listener->event;
      }
    }
  }

  variable_set('event_data_store_in_use', $events);
  return $events;
}

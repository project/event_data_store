<?php

/**
 * @file
 * event_data_store.forms.inc
 */

/**
 * Event store add/edit/clone form.
 */
function event_data_store_event_form($form, &$form_state, $event, $op = 'edit', $entity_type = NULL) {
  if ($op === 'clone') {
    $event->title .= ' (cloned)';
    $event->name = $entity_type . '_clone';
  }

  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => isset($event->title) ? $event->title : '',
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($event->name) ? $event->name : '',
    '#disabled' => isset($event->status) && empty($event->is_new) && (($event->status & ENTITY_IN_CODE) || ($event->status & ENTITY_FIXED)),
    '#machine_name' => array(
      'exists' => 'event_data_store_event_load_all',
      'source' => array('name'),
    ),
    '#description' => t('A unique machine-readable name for this event store. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['event'] = array(
    '#type' => 'select',
    '#title' => t('Event to trigger on'),
    '#options' => event_data_store_get_trigger_events(),
    '#default_value' => isset($event->event) ? $event->event : NULL,
    '#required' => TRUE,
  );

  $form['aggregate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aggregate data'),
    '#default_value' => isset($event->aggregate) ? $event->aggregate : 1,
  );

  $form['aggregate_behaviour'] = array(
    '#type' => 'select',
    '#title' => t('Aggregate behaviour'),
    '#options' => array(
      'tally' => t('Tally / Count'),
      'sum' => t('Sum'),
    ),
    '#default_value' => !empty($event->aggregate_behaviour) ? $event->aggregate_behaviour : 'tally',
    '#required' => TRUE,
    '#description' => t('Tally means to store the number of records created within the interval, sum is to take the tally number and add it to the previous count.'),
  );

  $form['aggregate_hourly'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aggregate hourly data'),
    '#default_value' => isset($event->aggregate_hourly) ? $event->aggregate_hourly : 1,
  );

  $form['aggregate_daily'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aggregate daily data'),
    '#default_value' => isset($event->aggregate_daily) ? $event->aggregate_daily : 1,
  );

  $form['aggregate_weekly'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aggregate weekly data'),
    '#default_value' => isset($event->aggregate_weekly) ? $event->aggregate_weekly : 1,
  );

  $form['aggregate_monthly'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aggregate monthly data'),
    '#default_value' => isset($event->aggregate_monthly) ? $event->aggregate_monthly : 1,
  );

  $form['aggregate_yearly'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aggregate yearly data'),
    '#default_value' => isset($event->aggregate_yearly) ? $event->aggregate_yearly : 0,
  );

  $day = 86400;
  $month = $day * 30;
  $form['prune_interval'] = array(
    '#type' => 'select',
    '#title' => t('Prune old data after'),
    '#options' => array('0' => t('Retain all data')) + drupal_map_assoc(array($day, $day * 7, $month, $month * 3, $month * 6, $day * 365), 'format_interval'),
    '#default_value' => isset($event->prune_interval) ? $event->prune_interval : 0,
  );

  $form['provide_permissions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide permissions'),
    '#default_value' => isset($event->provide_permissions) ? $event->provide_permissions : 0,
    '#description' => t('Whether or not to provide CRUD permissions and access checking around this store.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save event store'),
  );

  if ($op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete event store'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('event_data_store_event_form_submit_delete')
    );
  }

  $form['#submit'][] = 'event_data_store_event_form_submit';

  return $form;
}

/**
 * Form submit handler.
 */
function event_data_store_event_form_submit(&$form, &$form_state) {
  /**
   * Do the standard save operation.
   */
  $event = entity_ui_form_submit_build_entity($form, $form_state);
  entity_save('event_data_store_event', $event);

  /**
   * Clear ze caches.
   */
  event_data_store_build_in_use();

  $form_state['redirect'] = 'admin/structure/event_data_store';
}

/**
 * Form API submit callback for the delete button.
 */
function event_data_store_event_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/event_data_store/manage/' . $form_state['event_data_store_event']->name . '/delete';
}

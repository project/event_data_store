<?php

/**
 * @file
 * event_data_store.entity.inc
 */

/**
 * Implements hook_entity_info().
 */
function event_data_store_entity_info() {
  $entities = array();

  $entities['event_data_store_event'] = array(
    'label' => t('Event Data Store Event'),
    'plural label' => t('Event Data Store Entries'),
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'event_data_store_event',
    'fieldable' => FALSE,
    'bundle of' => 'event_data_store_entry',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'name',
      'label' => 'title',
    ),
    'module' => 'event_data_store',
    'admin ui' => array(
      'path' => 'admin/structure/event_data_store',
    ),
    'access callback' => 'event_data_store_event_access',
  );

  $entities['event_data_store_entry'] = array(
    'label' => t('Event Data Store Entry'),
    'plural label' => t('Event Data Store Entries'),
    'controller class' => 'EventDataStoreEntryController',
    'entity class' => 'EventDataStoreEntry',
    'base table' => 'event_data_store',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'eds_id',
      'bundle' => 'event_name',
    ),
    'bundle keys' => array(
      'bundle' => 'name',
    ),
    'bundles' => array(),
    'view modes' => array(
      'full' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
    ),
    'module' => 'event_data_store',
    'entity cache' => FALSE,
    'metatags' => FALSE,
    'access callback' => 'event_data_store_entry_access',
    'views controller class' => 'EntityDefaultViewsController',
  );

  $events = db_select('event_data_store_event', 'edse')
    ->fields('edse')
    ->execute()
    ->fetchAllAssoc('name');

  if (!empty($events)) {
    foreach ($events as $event_name => $event) {
      $entities['event_data_store_entry']['bundles'][$event_name] = array(
        'label' => check_plain($event->title),
        'admin' => array(
          'path' => 'admin/structure/event_data_store/manage/%event_data_store_event',
          'real path' => 'admin/structure/event_data_store/manage/' . $event_name,
          'bundle argument' => 4,
          'access arguments' => array('administer event_data_store'),
        ),
      );
    }
  }

  return $entities;
}

/**
 * Implements hook_entity_property_info_alter().
 *
 * We alter as an amount of these properties may already be populated.
 */
function event_data_store_entity_property_info_alter(&$info) {
  /**
   * Event/Type properties.
   */
  $event_properties = &$info['event_data_store_event']['properties'];

  $entry_properties['name'] = array(
    'label' => t('Machine name'),
    'description' => t('The machine name of the event.'),
    'schema field' => 'name',
  );
  $entry_properties['title'] = array(
    'label' => t('Event title'),
    'description' => t('The title of the event.'),
    'schema field' => 'name',
  );
  $entry_properties['event'] = array(
    'label' => t('Event'),
    'description' => t('The event that this store collects data on, e.g. user logins.'),
    'schema field' => 'event',
  );
  $entry_properties['aggregate'] = array(
    'label' => t('Generate aggregated data?'),
    'description' => t('Should this type aggregate data (tally/count data periodically).'),
    'type' => 'boolean',
    'schema field' => 'aggregate',
  );
  $entry_properties['aggregate_behaviour'] = array(
    'label' => t('Aggregation behaviour'),
    'description' => t('Whether to tally data or to count it. Tally means to store the number of records created within the interval, sum is to take the tally number and add it to the previous count.'),
    'schema field' => 'aggregate_behaviour',
  );
  $entry_properties['aggregate_hourly'] = array(
    'label' => t('Generate aggregated data?'),
    'description' => t('Should this type aggregate data hourly?'),
    'type' => 'boolean',
    'schema field' => 'aggregate_hourly',
  );
  $entry_properties['aggregate_daily'] = array(
    'label' => t('Generate daily aggregated data?'),
    'description' => t('Should this type aggregate data daily?'),
    'type' => 'boolean',
    'schema field' => 'aggregate_daily',
  );
  $entry_properties['aggregate_weekly'] = array(
    'label' => t('Generate weekly aggregated data?'),
    'description' => t('Should this type aggregate data weekly?'),
    'type' => 'boolean',
    'schema field' => 'aggregate_weekly',
  );
  $entry_properties['aggregate_monthly'] = array(
    'label' => t('Generate aggregated data?'),
    'description' => t('Should this type aggregate data monthly?'),
    'type' => 'boolean',
    'schema field' => 'aggregate_monthly',
  );
  $entry_properties['aggregate_yearly'] = array(
    'label' => t('Generate aggregated data?'),
    'description' => t('Should this type aggregate data yearly?'),
    'type' => 'boolean',
    'schema field' => 'aggregate_yearly',
  );
  $entry_properties['prune_interval'] = array(
    'label' => t("Prune interval"),
    'type' => 'integer',
    'description' => t("How often to delete individual storage records (not aggregated data)."),
    'schema field' => 'prune_interval',
  );
  $entry_properties['provide_permissions'] = array(
    'label' => t('Provide permissions?'),
    'description' => t('Whether or not to provide CRUD permissions for this event store.'),
    'type' => 'boolean',
    'schema field' => 'provide_permissions',
  );

  /**
   * Entry properties.
   */
  $entry_properties = &$info['event_data_store_entry']['properties'];

  $entry_properties['eds_id'] = array(
    'label' => t("Entry ID"),
    'type' => 'integer',
    'description' => t("The unique ID of the entry."),
    'schema field' => 'eds_id',
  );
  $entry_properties['event_name'] = array(
    'label' => t('Event name'),
    'description' => t('The date the conversation was created.'),
    'schema field' => 'event_name',
  );
  $entry_properties['sub_type'] = array(
    'label' => t('Sub type'),
    'description' => t('A sub type which applies to the entry.'),
    'schema field' => 'sub_type',
  );
  $entry_properties['uid'] = array(
    'label' => t('Trigger user'),
    'type' => 'user',
    'description' => t('The user who triggered the event.'),
    'required' => TRUE,
    'schema field' => 'uid',
  );
  $entry_properties['timestamp'] = array(
    'label' => t('Event date'),
    'type' => 'date',
    'description' => t('The date the event occurred.'),
    'schema field' => 'timestamp',
  );
  $entry_properties['user_agent'] = array(
    'label' => t('Record User Agent data'),
    'description' => t('The triggering users user agent data.'),
    'schema field' => 'user_agent',
  );
  $entry_properties['ip_address'] = array(
    'label' => t('IP Address'),
    'description' => t('The hostname or IP address from where the event was triggered.'),
    'schema field' => 'ip_address',
  );
  $entry_properties['entity_type'] = array(
    'label' => t('Entity type'),
    'description' => t('Type of the entity against which an event happened. Advanced usage only.'),
    'schema field' => 'entity_type',
  );
  $entry_properties['entity_id'] = array(
    'label' => t('Entity ID'),
    'description' => t('ID of the entity against which an event happened. Advanced usage only.'),
    'type' => 'integer',
    'schema field' => 'entity_id',
  );
}

/**
 * Load a single event store.
 *
 * @param string $event_store
 *   The event identifier.
 */
function event_data_store_event_load($event_store) {
  return event_data_store_event_load_all($event_store);
}

/**
 * Load a all events.
 *
 * @param string $event_store
 *   An optional event store name.
 */
function event_data_store_event_load_all($event_store = NULL) {
  $events = entity_load_multiple_by_name('event_data_store_event', isset($event_store) ? array($event_store) : FALSE);

  return isset($event_store) ? reset($events) : $events;
}

/**
 * Return a flat array of events to be used with form api as options.
 */
function event_data_store_event_load_all_flat() {
  $events = event_data_store_event_load_all();

  $options = array();
  foreach ($events as $type => $info) {
    $options[$type] = t($info->title);
  }
  asort($options);

  return $options;
}

/**
 * Custom entity getter callback used from metadata info.
 */
function event_data_store_entry_get_entity($data, array $options, $name, $type, $info) {
  return entity_load_single($data->entity_type, $data->entity_id);
}

/**
 * Load an entry.
 */
function event_data_store_entry_load($entry_id, $uid = NULL, $reset = FALSE) {
  $conversations = event_data_store_entry_load_multiple(array($entry_id), $uid, array(), $reset);
  return reset($conversations);
}

/**
 * Load a series of entries.
 */
function event_data_store_entry_load_multiple(array $entry_ids = array(), $uid = NULL, array $conditions = array(), $reset = FALSE) {
  $controller = entity_get_controller('event_data_store_entry');
  if ($reset) {
    $controller->resetCache();
  }

  return $controller->load($entry_ids, $conditions);
}

/**
 * Check if a user has access to perform an operation against an event store.
 */
function event_data_store_event_access($op, $entity, $account, $entity_type) {
  return user_access('administer event_data_store', $account);
}

/**
 * Access callback for an individual entry record.
 *
 * @param $op
 *   The operation to be performed on the thread. Possible values are:
 *   - "view"
 *   - "update"
 *   - "delete"
 *   - "create"
 * @param $entity
 *   The entry object on which the operation is to be performed.
 * @param $account
 *   Optional, a user object representing the user for whom the operation is to
 *   be performed. Determines access for a user other than the current user.
 *
 * @return
 *   TRUE if the operation may be performed, FALSE otherwise.
 */
function event_data_store_entry_access($op, $entity, $account, $entity_type) {
  $events = event_data_store_event_load_all();

  if ($events[$entity->event_name]->provide_permissions) {
    if (user_access("event_data_store administer {$entity->event_name}", $account)) {
      return TRUE;
    }

    switch ($op) {
      case 'view':
        if (user_access("event_data_store view {$entity->event_name}", $account)) {
          return TRUE;
        }
        break;

      case 'create':
        if (user_access("event_data_store create {$entity->event_name}", $account)) {
          return TRUE;
        }
        break;

      case 'update':
        if (user_access("event_data_store update any {$entity->event_name}", $account) || $account->uid == $entity->uid && user_access("event_data_store update own {$entity->event_name}", $account)) {
          return TRUE;
        }
        break;

      case 'delete':
        if (user_access("event_data_store delete any {$entity->event_name}", $account) || $account->uid == $entity->uid && user_access("event_data_store delete own {$entity->event_name}", $account)) {
          return TRUE;
        }
        break;
    }
  }
  /**
   * Not providing any specific provision for access control therefore whatever
   * listing is responsible for enforcing access.
   */
  else {
    return TRUE;
  }
}

<?php

/**
 * @file
 * Provide additional Views content.
 */


/**
 * Implements hook_views_data().
 */
function event_data_store_views_data() {
  $data = array();

  /**
   * The individual records are integrated with Views via Entity API.
   */

  /**
   * Integrate the aggregated data store.
   */
  $data['event_data_store_agg'] = array(
    'table' => array(
      'base' => array(
        'field' => 'edsa_id',
        'title' => t('Event Data Store Aggregated records'),
        'help' => t('Query the aggregated stored data relating to events.'),
      ),
      'group' => t('Event Data Store'),
    ),
    'edsa_id' => array(
      'title' => t('Record ID'),
      'help' => t('The unique ID of this specific record.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        'allow empty' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'event_name' => array(
      'title' => t('Event name'),
      'help' => t('The name of the data store event.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'event_data_store_views_handler_filter_event_name',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'period' => array(
      'title' => t('Record interval'),
      'help' => t('The interval for the aggregated record: hourly, daily, weekly, monthly.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'event_data_store_views_handler_filter_agg_period',
        'allow empty' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'timestamp' => array(
      'title' => t('Event date'),
      'help' => t('The date the event occurred.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
        'is date' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'date_views_filter_handler',
        'is date' => TRUE,
      ),
      'argument' => array(
        'handler' => 'date_views_argument_handler_simple',
        'empty field name' => t('Undated'),
        'is date' => TRUE,
      ),
    ),
    'value' => array(
      'title' => t('Record data/value'),
      'help' => t('The aggregated value for this store.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
        'allow empty' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data().
 */
function event_data_store_views_data_alter(&$data) {
  // Setup our Entity joins.
  foreach (entity_get_info() as $entity_type => $entity_info) {
    $label = $entity_type === t('node') ? t('Content') : check_plain($entity_info['label']);

    $data[$entity_info['base table']]['event_data_store_rel'] = array(
      'group' => t('Event Data Store'),
      'title' => $label,
      'help' => t('Join to Event Data Store entries for @types.', array('@type' => $label)),
      'relationship' => array(
        'handler' => 'EventDataStoreViewsHandlerRelationship',
        'label' => 'event_data_store',
        'base' => 'event_data_store',
        'base field' => 'entity_id',
        'entity type' => $entity_type,
        'relationship field' => $entity_info['entity keys']['id'],
      ),
    );
  }
}

<?php

/**
 * @file
 * Contains EventDataStoreViewsHandlerRelationship.
 */

class EventDataStoreViewsHandlerRelationship extends views_handler_relationship {
  /**
   * Init handler to let relationships live on tables other than
   * the table they operate on.
   */
  function init(&$view, &$options) {
    parent::init($view, $options);
    if (isset($this->definition['relationship table'])) {
      $this->table = $this->definition['relationship table'];
    }
    if (isset($this->definition['entity type'])) {
      $this->entity_type = $this->definition['entity type'];
    }
    if (isset($this->definition['relationship field'])) {
      // Set both real_field and field so custom handler
      // can rely on the old field value.
      $this->real_field = $this->field = $this->definition['relationship field'];
    }
  }

  /**
   * Overrides views_handler_relationship::option_definition().
   *
   * Add in option definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['events'] = array('default' => array());
    return $options;
  }

  /**
   * Overrides views_handler_relationship::options_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['events'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Events'),
      '#options' => event_data_store_event_load_all_flat(),
      '#default_value' => $this->options['events'],
    );
  }

  /**
   * Overrides views_handler_relationship::query().
   *
   * Called to implement a relationship in a query.
   */
  function query() {
    /**
     * Include the extra where stuff.
     */
    $events = array_filter($this->options['events']);
    if (!empty($events)) {
      $this->definition['extra'][] = array(
        'field' => 'event_name',
        'value' => $events,
      );
    }

    $this->definition['extra'][] = array(
      'field' => 'entity_type',
      'value' => $this->entity_type,
      'numeric' => FALSE,
    );

    /**
     * Do the standard relationship query construction.
     */
    parent::query();
  }
}
